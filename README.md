# CNFModelChecker

A tool to check if the given model satisfies a given CNF formula (both in DIMACs format).  It also provides information about the number of seen/expected variables, seen/expected clauses, and satisfied clauses.  The implementation of the tool in different languages is available in corresponding folder -- _Go_, and _Kotlin_.


## Attribution

Copyright (c) 2017, Venkatesh-Prasad Ranganath

Licensed under [BSD 3-clause "New" or "Revised" License](https://choosealicense.com/licenses/bsd-3-clause/)

**Authors:** Venkatesh-Prasad Ranganath
