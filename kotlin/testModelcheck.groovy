/*
 * Copyright (c) 2019 Venkatesh-Prasad Ranganath
 *
 * BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath (rvprasad)
 */

@Grab(group='org.junit.jupiter', module='junit-jupiter', version='5.6.2')

import org.junit.jupiter.api.Test

class TestModelCheck {
    def executeModelCheckerWithArgs(args) {
        final check = (['kscript', 'modelcheck.kts'] + args).join(' ')
        final outStream = new ByteArrayOutputStream()
        final proc = check.execute()
        proc.consumeProcessOutputStream(new PrintStream(outStream))
        proc.waitFor()
        final sep = System.getProperty('line.separator')
        return outStream.toString().split(sep) as List
    }

    @Test
    void testSatisfiableCNFAndCompleteModel() {
        final args = ['-c', '../test-inputs/satisfiable.cnf',
                      '-m', '../test-inputs/satisfiable-complete.model']
        final out = executeModelCheckerWithArgs(args)
        assert out.size() == 4
        assert out[0] == 'Variables 7 seen 7 expected'
        assert out[1] == 'Clauses 11 seen 11 expected'
        assert out[2] == 'Satisfied Clauses 11'
        assert out[3] == 'Model satisfies the formula'
    }

    @Test
    void testSatisfiableCNFAndIncompleteModel() {
        final args = ['-c', '../test-inputs/satisfiable.cnf',
                      '-m', '../test-inputs/satisfiable-incomplete.model']
        final out = executeModelCheckerWithArgs(args)
        assert out.size() == 8
        assert out[0] == 'Literals -6 on line 8 are unassigned.'
        assert out[1] == 'Literals -6 on line 9 are unassigned.'
        assert out[2] == 'Literals -6 on line 10 are unassigned.'
        assert out[3] == 'Literals 6 on line 13 are unassigned.'
        assert out[4] == 'Variables 7 seen 7 expected'
        assert out[5] == 'Clauses 11 seen 11 expected'
        assert out[6] == 'Satisfied Clauses 11'
        assert out[7] == 'Model satisfies the formula'
    }

    @Test
    void testSatisfiableCNFAndWrongVerdictModel() {
        final args = ['-c', '../test-inputs/satisfiable.cnf',
                      '-m', '../test-inputs/satisfiable-wrong-verdict.model']
        final out = executeModelCheckerWithArgs(args)
        assert out.size() == 5
        assert out[0] == 'Variables 7 seen 7 expected'
        assert out[1] == 'Clauses 11 seen 11 expected'
        assert out[2] == 'Satisfied Clauses 11'
        assert out[3] == 'Model satisfies the formula'
        assert out[4] == 'Found verdict (satisfied=true) does not agree ' +
                'with specified verdict (satisfied=false)'
    }

    @Test
    void testUnsatisfiableCNFAndCompleteModel() {
        final args = ['-c', '../test-inputs/unsatisfiable.cnf',
                      '-m', '../test-inputs/unsatisfiable-complete.model']
        final out = executeModelCheckerWithArgs(args)
        assert out.size() == 4
        assert out[0] == 'Clause on line 4 is false.'
        assert out[1] == 'Variables 2 seen 2 expected'
        assert out[2] == 'Clauses 4 seen 4 expected'
        assert out[3] == 'Satisfied Clauses 3'
    }

    @Test
    void testSatisfiableMoreThanNBVarVarsCNFAndCompleteModel() {
        final args = ['-c', '../test-inputs/satisfiable-more-than-nbvar-vars.cnf',
                      '-m', '../test-inputs/satisfiable-complete.model']
        final out = executeModelCheckerWithArgs(args)
        assert out.size() == 5
        assert out[0] == 'Literals 8 on line 11 are unassigned.'
        assert out[1] == 'Variables 8 seen 7 expected'
        assert out[2] == 'Clauses 11 seen 11 expected'
        assert out[3] == 'Satisfied Clauses 11'
        assert out[4] == 'Model satisfies the formula'
    }

    @Test
    void testSatisfiableLessThanNBVarVarsCNFAndCompleteModel() {
        final args = ['-c', '../test-inputs/satisfiable-less-than-nbvar-vars.cnf',
                      '-m', '../test-inputs/satisfiable-complete.model']
        final out = executeModelCheckerWithArgs(args)
        assert out.size() == 4
        assert out[0] == 'Variables 7 seen 6 expected'
        assert out[1] == 'Clauses 11 seen 11 expected'
        assert out[2] == 'Satisfied Clauses 11'
        assert out[3] == 'Model satisfies the formula'
    }

    @Test
    void testSatisfiableMoreThanNBClausesClausesCNFAndCompleteModel() {
        final args = ['-c', '../test-inputs/satisfiable-more-than-nbclauses-clauses.cnf',
                      '-m', '../test-inputs/satisfiable-complete.model']
        final out = executeModelCheckerWithArgs(args)
        assert out.size() == 4
        assert out[0] == 'Variables 7 seen 7 expected'
        assert out[1] == 'Clauses 12 seen 11 expected'
        assert out[2] == 'Satisfied Clauses 12'
        assert out[3] == 'Model satisfies the formula'
    }

    @Test
    void testSatisfiableLessThanNBClausesClausesCNFAndCompleteModel() {
        final args = ['-c', '../test-inputs/satisfiable-less-than-nbclauses-clauses.cnf',
                      '-m', '../test-inputs/satisfiable-complete.model']
        final out = executeModelCheckerWithArgs(args)
        assert out.size() == 4
        assert out[0] == 'Variables 7 seen 7 expected'
        assert out[1] == 'Clauses 10 seen 11 expected'
        assert out[2] == 'Satisfied Clauses 10'
        assert out[3] == 'Model satisfies the formula'
    }

    @Test
    void testSatisfiableCNFAndCompleteInconsistentModel() {
        final args = ['-c', '../test-inputs/satisfiable.cnf',
                      '-m', '../test-inputs/satisfiable-complete-inconsistent.model']
        final out = executeModelCheckerWithArgs(args)
        assert out.size() == 1
        assert out[0] == 'Variable 7 has conflicting model on line 3.'
    }

    @Test
    void testSatisfiableCNFAndCompleteInconsistentModel2() {
        final args = ['-c', '../test-inputs/satisfiable.cnf',
                      '-m', '../test-inputs/satisfiable-complete-inconsistent2.model']
        final out = executeModelCheckerWithArgs(args)
        assert out.size() == 2
        assert out[0] == 'Variable 7 has conflicting model on line 3.'
        assert out[1] == 'Variable 6 has conflicting model on line 4.'
    }
}
