# CNFModelChecker

A tool to check if the given model satisfies the given CNF formula.  It provides information about the number of seen/expected variables, seen/expected clauses, and satisfied clauses.  

**Note:** This is an adaptation of the Groovy implementation that was retired.


## Requirements
- JDK 1.8+.  We use JDKs from the following sources.
    - [Oracle](http://www.oracle.com/technetwork/java/javase/%20downloads/index.html)
    - [Azul Systems](https://www.azul.com/products/zulu/)
- [kscript 2.8.0](https://github.com/holgerbrandl/kscript)
- [For Development] [Kotlin 1.3.41](https://kotlinlang.org/)
- [For Development] [Clikt 2.1.0](https://ajalt.github.io/clikt/)
- [For Development] [Kscript Annotations 1.4](https://github.com/holgerbrandl/kscript-annotations)
- [For Testing] [Groovy 2.5.7](http://www.groovy-lang.org/)

Use [SDKMan](http://sdkman.io/) or [Posh-GVM](http://github.com/flofreud/posh-gvm) to get Groovy, Java, Kotlin, and Kscript.


## Usage
- Run `kscript modelcheck.kts -c <cnf file> -m <model file>`
 
```
Usage: kscript modelcheck.kts [OPTIONS]

Options:
  -c, --cnf TEXT    CNF file in DIMACS format
                    (http://www.satcompetition.org/2009/format-benchmarks2009.html)
  -m, --model TEXT  Model in DIMACS format
                    (http://www.satcompetition.org/2009/format-solvers2009.html)
  -h, --help        Show this message and exit
```

## Specifics
- Ungrounded literals will be flagged.
- Unsatisfied clauses will be flagged.
- Inconsistent models will be flagged.
- A clause is deemed as satisfied if any literal in it is satisfied; independent of other literals not being grounded.
- In models, in values (_v_) lines, variables to the right of 0 will not be considered; they will not be flagged as well.
- In formulae, in clause lines, variables to the right of 0 will not be considered; they will not be flagged as well.
- All clauses and variables are processed independent of the expectation stated in parameter (_p_) line.
- Both specified/expected and seen number of variables and clauses are reported.

 
## Attribution

Copyright (c) 2019, Venkatesh-Prasad Ranganath

Licensed under BSD 3-clause "New" or "Revised" License (https://choosealicense.com/licenses/bsd-3-clause/)

Authors: Venkatesh-Prasad Ranganath
