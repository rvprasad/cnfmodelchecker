/*
 * Copyright (c) 2019, Venkatesh-Prasad Ranganath
 *
 * BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath (rvprasad)
 */

@file:DependsOn("com.github.ajalt:clikt:2.1.0")

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import java.io.File
import kotlin.text.Regex
import kotlin.system.exitProcess

private typealias Assignment = Set<Int>

internal class CommandLine: CliktCommand(name = "kscript modelcheck.kts",
        printHelpOnEmptyArgs = true) {
    private val cnf by option("-c", "--cnf", help = "CNF file in DIMACS format " +
            "(http://www.satcompetition.org/2009/format-benchmarks2009.html)").required()
    private val model by option("-m", "--model", help = "Model file in DIMACS format " +
            "(http://www.satcompetition.org/2009/format-solvers2009.html)").required()

    override fun run() {
        val (conflictingModel, specifiedAsSatisfied, model) = readModel(model)
        if (conflictingModel)
            exitProcess(-2)
        else {
            processCNF(cnf, model).run {
                println("Variables $numSeenVars seen $numExpectedVars expected")
                println("Clauses $numSeenClauses seen $numExpectedClauses expected")
                println("Satisfied Clauses $numSatisfiedClauses")

                if (satisfied)
                    println("Model satisfies the formula")

                if (satisfied != specifiedAsSatisfied)
                    println(
                        "Found verdict (satisfied=$satisfied) does not agree with specified verdict " +
                                "(satisfied=$specifiedAsSatisfied)"
                    )
            }
        }
    }

    private fun tokenize(line: String) = line.trim().split(' ').filter(String::isNotBlank)

    private fun readModel(modelFilename: String): Triple<Boolean, Boolean, Assignment> {
        var conflictingModel = false
        var specifiedAsSatisfied = false
        val satRegex = Regex("^s SATISFIABLE")
        val varRegex = Regex("^v .*")
        val model = HashSet<Int>()
        File(modelFilename).bufferedReader().lineSequence()
            .mapIndexed { idx, line ->
                specifiedAsSatisfied = specifiedAsSatisfied || satRegex.containsMatchIn(line)
                Pair(idx + 1, line)
            }
            .filter { varRegex.containsMatchIn(it.second) }
            .forEach { (lineNum, line) ->
                val vals = tokenize(line).filter { it != "v" }.map(String::toInt).takeWhile { it != 0 }
                val conflictingValues = vals.filter { -it in model || -it in vals }.map(Math::abs).distinct()
                conflictingModel = conflictingModel || conflictingValues.isNotEmpty()
                conflictingValues.forEach {
                    println("Variable ${Math.abs(it)} has conflicting model on line $lineNum.")
                }
                model.addAll(vals)
            }
        return Triple(conflictingModel, specifiedAsSatisfied, model)
    }

    private fun processCNF(cnfFilename: String, model: Assignment): Result {
        val seenVars = HashSet<Int>()
        val infoRegex = Regex("^p cnf \\d+ \\d+.*")
        val clauseRegex = Regex("^\\s*-?\\d.*")
        val result = Result()
        File(cnfFilename).bufferedReader().lineSequence()
            .forEachIndexed { idx, line ->
                result.apply {
                    val lineNum = idx + 1
                    val tokens = tokenize(line)
                    if (infoRegex.containsMatchIn(line)) {
                        numExpectedVars = tokens[2].toInt()
                        numExpectedClauses = tokens[3].toInt()
                    } else if (clauseRegex.containsMatchIn(line)) {
                        numSeenClauses++
                        val clause = tokens.map(String::toInt).takeWhile { it != 0 }
                        seenVars += clause.map(Math::abs)
                        val unresolvedLiterals = clause.filter { it !in model && -it !in model }
                        if (unresolvedLiterals.isNotEmpty())
                            println("Literals ${unresolvedLiterals.joinToString(",")} on line $lineNum are unassigned.")

                        when {
                            clause.any { it in model } -> numSatisfiedClauses++
                            clause.all { -it in model } -> {
                                println("Clause on line $lineNum is false.")
                                satisfied = false
                            }
                            else -> {
                                println("Clause on line $lineNum is unresolved.")
                                satisfied = false
                            }
                        }
                    }
                }
            }

        result.numSeenVars = seenVars.size
        return result
    }

    private data class Result(
        var numExpectedClauses: Int = 0,
        var numExpectedVars: Int = 0,
        var numSeenClauses: Int = 0,
        var numSeenVars: Int = 0,
        var numSatisfiedClauses: Int = 0,
        var satisfied: Boolean = true
    )
}

CommandLine().main(args)
