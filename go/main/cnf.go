/*
 * Copyright (c) 2017, Venkatesh-Prasad Ranganath
 *
 * BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath
 */

package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

type CNFData struct {
	expectedNumOfClauses  int
	seenNumOfClauses      int
	expectedNumOfVars     int
	seenVars              map[int]bool
	satisfiedNumOfClauses int
}

func processClause(tokens []string, lineNum int, cnfData *CNFData,
	lit2value map[int]bool) statusCode {
	_lits, _err := convertToInt(tokens)
	if _err != OK {
		fmt.Printf("Invalid line %d in CNF.\n", lineNum)
		return _err
	}

	if len(_lits) > 1 {
		cnfData.seenNumOfClauses++
	}

	_satisfied := false
	_notGrounded := false
	for _, _lit := range _lits {
		if _lit == 0 {
			break
		}
		cnfData.seenVars[abs(_lit)] = true
		if _value, _ok := lit2value[_lit]; _ok {
			_satisfied = _satisfied || _value
		} else {
			fmt.Printf("Literals %d at line %d is unassigned.\n", _lit,
				lineNum)
			_notGrounded = true
		}
	}

	if _satisfied {
		cnfData.satisfiedNumOfClauses++
	} else {
		if _notGrounded {
			fmt.Printf("Clause at line %d is unresolved.\n", lineNum)
		} else {
			fmt.Printf("Clause at line %d is false.\n", lineNum)
		}
	}

	return OK
}

func processPLine(tokens []string, lineNum int, cnfData *CNFData) statusCode {
	if tokens[1] != "cnf" {
		fmt.Println("Not CNF")
		return NotCNF
	}

	_tmp1, _err := strconv.ParseInt(tokens[2], 10, 0)
	if _err != nil {
		fmt.Printf("Could not convert %s to int at line %d.\n",
			tokens[2], lineNum)
		return ErrorConvertingString
	}

	cnfData.expectedNumOfVars = int(_tmp1)

	_tmp1, _err = strconv.ParseInt(tokens[3], 10, 0)
	if _err != nil {
		fmt.Printf("Could not convert %s to int at line %d.\n",
			tokens[2], lineNum)
		return ErrorConvertingString
	}

	cnfData.expectedNumOfClauses = int(_tmp1)
	return OK
}

func processCNF(cnfFileName string, lit2value map[int]bool,
	specifiedAsSatisfied bool) statusCode {
	_cnfScanner, _cnfFile, _err := getScanner(cnfFileName)
	if _err != OK {
		os.Exit(int(_err))
	}

	defer _cnfFile.Close()

	var cnfData CNFData
	cnfData.seenVars = map[int]bool{}
	_lineNum := 1
	for _cnfScanner.Scan() {
		_line := _cnfScanner.Text()

		if _tokens, _status := tokenize(strings.TrimSpace(_line)); _status == OK && len(_tokens) > 0 {
			switch _tokens[0] {
			case "p":
				if _status := processPLine(_tokens, _lineNum, &cnfData); _status != OK {
					return _status
				}
			case "c":
				// ignore the comment
			default:
				if _status := processClause(_tokens, _lineNum, &cnfData, lit2value); _status != OK {
					return _status
				}

			}
		}

		_lineNum++
	}

	fmt.Printf("Variables %d seen %d expected\n", len(cnfData.seenVars),
		cnfData.expectedNumOfVars)
	fmt.Printf("Clauses %d seen %d expected\n", cnfData.seenNumOfClauses,
		cnfData.expectedNumOfClauses)
	fmt.Println("Satisfied Clauses", cnfData.satisfiedNumOfClauses)
	_satisfied := cnfData.seenNumOfClauses == cnfData.satisfiedNumOfClauses
	if _satisfied {
		fmt.Println("Model satisfies the formula")
	}

	if _satisfied != specifiedAsSatisfied {
		fmt.Printf("Found verdict (satisfied=%v) does not agree with "+
			"specified verdict (satisfied=%v)\n", _satisfied,
			specifiedAsSatisfied)
	}

	return OK
}
