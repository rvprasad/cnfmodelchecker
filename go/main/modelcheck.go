/*
 * Copyright (c) 2017, Venkatesh-Prasad Ranganath
 *
 * BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath
 */

package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"regexp"
	"strconv"
)

type statusCode int

const (
	OK = iota
	InsufficientCLIOptions
	FileNotFound
	FileReadError
	ErrorCompilingRegex
	ErrorConvertingString
	InvalidFileFormat
	InvalidModel
	NotCNF
)

func getScanner(fileName string) (scanner *bufio.Scanner, _file *os.File,
	status statusCode) {
	status = OK
	var _err error
	_file, _err = os.Open(fileName)
	if _pathError, _ok := _err.(*os.PathError); _ok {
		fmt.Printf("Could not open %s (%s).\n", _pathError.Path, _pathError.Err)
		status = FileNotFound
		return
	}

	_tmp1 := io.Reader(_file)
	scanner = bufio.NewScanner(_tmp1)
	return
}

func tokenize(str string) (tokens []string, status statusCode) {
	if len(str) < 1 {
		status = OK
		tokens = []string{}
	} else if _regexp, _err := regexp.Compile(`\s+`); _err == nil {
		tokens = _regexp.Split(str, -1)
		status = OK
	} else {
		fmt.Println("Error compiling regex.")
		status = ErrorCompilingRegex
	}
	return
}

func abs(i int) int {
	if i < 0 {
		return -i
	}
	return i
}

func convertToInt(tokens []string) (ints []int, status statusCode) {
	ints = []int{}
	status = OK
	for _, _elem := range tokens {
		_tmp1, _err := strconv.ParseInt(_elem, 10, 0)
		if _err != nil {
			fmt.Printf("Could not convert %s to int.\n", _elem)
			status = ErrorConvertingString
			return
		}
		ints = append(ints, int(_tmp1))
	}
	return
}

func main() {
	_cnfOption := flag.String("cnf", "",
		`CNF file in DIMACS format (http://www.satcompetition.org/2009/format-benchmarks2009.html)`)
	_modelOption := flag.String("model", "",
		`Model in DIMACS format (http://www.satcompetition.org/2009/format-solvers2009.html)`)
	flag.Parse()

	if *_cnfOption == "" || *_modelOption == "" {
		flag.PrintDefaults()
		os.Exit(InsufficientCLIOptions)
	}

	var _status statusCode
	if _lit2value, _specifiedAsSatisfied, _status :=
		processModel(*_modelOption); _status == OK {
		_status = processCNF(*_cnfOption, _lit2value, _specifiedAsSatisfied)
	}
	os.Exit(int(_status))
}
