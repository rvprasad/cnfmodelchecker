/*
 * Copyright (c) 2017, Venkatesh-Prasad Ranganath
 *
 * BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath
 */

package main

import (
	"bufio"
	"io"
	"os"
	"os/exec"
	"strings"
	"testing"
)

const (
	modelCheckerPath                       = `bin/modelcheck`
	satisfiableCompleteModel               = `../test-inputs/satisfiable-complete.model`
	satisfiableIncompleteModel             = `../test-inputs/satisfiable-incomplete.model`
	satisfiableWrongVerdictModel           = `../test-inputs/satisfiable-wrong-verdict.model`
	satisfiableCompleteInconsistentModel   = `../test-inputs/satisfiable-complete-inconsistent.model`
	satisfiableMoreThanNBVarVarsCNF        = `../test-inputs/satisfiable-more-than-nbvar-vars.cnf`
	satisfiableLessThanNBVarVarsCNF        = `../test-inputs/satisfiable-less-than-nbvar-vars.cnf`
	satisfiableMoreThanNBClausesClausesCNF = `../test-inputs/satisfiable-more-than-nbclauses-clauses.cnf`
	satisfiableLessThanNBClausesClausesCNF = `../test-inputs/satisfiable-less-than-nbclauses-clauses.cnf`
	satisfiableCnf                         = `../test-inputs/satisfiable.cnf`
	unsatisfiableCompleteModel             = `../test-inputs/unsatisfiable-complete.model`
	unsatisfiableCnf                       = `../test-inputs/unsatisfiable.cnf`
)

func executeChecker(cnfFileName string, modelFileName string, t *testing.T) (out []string, success bool) {
	_t, _ := os.Getwd()
	t.Log(_t)
	_cmd := exec.Command(modelCheckerPath, `-model`, modelFileName, `-cnf`, cnfFileName)
	_tmp1, _err := _cmd.StdoutPipe()
	out = nil
	success = false
	if _err != nil {
		t.Fatal("Failed to trap output of modelchecking: ", _err)
		return
	}

	if _err := _cmd.Start(); _err != nil {
		t.Fatal("Error executing modelchecker: ", _err)
		return
	}

	out = []string{}
	_stdout := bufio.NewReader(_tmp1)
	for {
		_line, _err := _stdout.ReadString('\n')
		out = append(out, _line)
		if _err != nil {
			if _err == io.EOF {
				success = true
			} else {
				t.Fatal("Error while reading output from modelchecking: ", _err)
				success = false
			}
			return
		}
	}

	if _err := _cmd.Wait(); _err != nil {
		t.Fatal("Error while waiting for modelchecker: ", _err)
		success = false
	}
	return
}

func TestSatisfiableCNFAndCompleteModel(t *testing.T) {
	_expected := []string{
		`Variables 7 seen 7 expected`,
		`Clauses 11 seen 11 expected`,
		`Satisfied Clauses 11`,
		`Model satisfies the formula`,
	}
	if _out, _ok := executeChecker(satisfiableCnf, satisfiableCompleteModel, t); _ok {
		for _i, _line := range _out {
			if len(_line) > 0 && _expected[_i] != strings.TrimSpace(_line) {
				t.Fatalf("Expected: %s / Observed: %s", _expected[_i], _line)
			}
		}
	} else {
		t.Fatal("ModelChecker execution failed")
	}
}

func TestSatisfiableCNFAndIncompleteModel(t *testing.T) {
	_expected := []string{
		`Literals -6 at line 8 is unassigned.`,
		`Literals -6 at line 9 is unassigned.`,
		`Literals -6 at line 10 is unassigned.`,
		`Literals 6 at line 13 is unassigned.`,
		`Variables 7 seen 7 expected`,
		`Clauses 11 seen 11 expected`,
		`Satisfied Clauses 11`,
		`Model satisfies the formula`,
	}
	if _out, _ok := executeChecker(satisfiableCnf, satisfiableIncompleteModel, t); _ok {
		for _i, _line := range _out {
			if len(_line) > 0 && _expected[_i] != strings.TrimSpace(_line) {
				t.Fatalf("Expected: %s / Observed: %s", _expected[_i], _line)
			}
		}
	} else {
		t.Fatal("ModelChecker execution failed")
	}
}

func TestSatisfiableCNFAndWrongVerdictModel(t *testing.T) {
	_expected := []string{
		`Variables 7 seen 7 expected`,
		`Clauses 11 seen 11 expected`,
		`Satisfied Clauses 11`,
		`Model satisfies the formula`,
		`Found verdict (satisfied=true) does not agree with specified verdict (satisfied=false)`,
	}
	if _out, _ok := executeChecker(satisfiableCnf, satisfiableWrongVerdictModel, t); _ok {
		for _i, _line := range _out {
			if len(_line) > 0 && _expected[_i] != strings.TrimSpace(_line) {
				t.Fatalf("Expected: %s / Observed: %s", _expected[_i], _line)
			}
		}
	} else {
		t.Fatal("ModelChecker execution failed")
	}
}

func TestUnsatisfiableCNFAndCompleteModel(t *testing.T) {
	_expected := []string{
		`Clause at line 4 is false.`,
		`Variables 2 seen 2 expected`,
		`Clauses 4 seen 4 expected`,
		`Satisfied Clauses 3`,
	}
	if _out, _ok := executeChecker(unsatisfiableCnf, unsatisfiableCompleteModel, t); _ok {
		for _i, _line := range _out {
			if len(_line) > 0 && _expected[_i] != strings.TrimSpace(_line) {
				t.Fatalf("Expected: %s / Observed: %s", _expected[_i], _line)
			}
		}
	} else {
		t.Fatal("ModelChecker execution failed")
	}
}

func TestSatisfiableMoreThanNBVarVarsCNFAndCompleteModel(t *testing.T) {
	_expected := []string{
		`Literals 8 at line 11 is unassigned.`,
		`Variables 8 seen 7 expected`,
		`Clauses 11 seen 11 expected`,
		`Satisfied Clauses 11`,
		`Model satisfies the formula`,
	}
	if _out, _ok := executeChecker(satisfiableMoreThanNBVarVarsCNF, satisfiableCompleteModel, t); _ok {
		for _i, _line := range _out {
			if len(_line) > 0 && _expected[_i] != strings.TrimSpace(_line) {
				t.Fatalf("Expected: %s / Observed: %s", _expected[_i], _line)
			}
		}
	} else {
		t.Fatal("ModelChecker execution failed")
	}
}

func TestSatisfiableLessThanNBVarVarsCNFAndCompleteModel(t *testing.T) {
	_expected := []string{
		`Variables 7 seen 6 expected`,
		`Clauses 11 seen 11 expected`,
		`Satisfied Clauses 11`,
		`Model satisfies the formula`,
	}
	if _out, _ok := executeChecker(satisfiableLessThanNBVarVarsCNF, satisfiableCompleteModel, t); _ok {
		for _i, _line := range _out {
			if len(_line) > 0 && _expected[_i] != strings.TrimSpace(_line) {
				t.Fatalf("Expected: %s / Observed: %s", _expected[_i], _line)
			}
		}
	} else {
		t.Fatal("ModelChecker execution failed")
	}
}

func TestSatisfiableMoreThanNBClausesClausesCNFAndCompleteModel(t *testing.T) {
	_expected := []string{
		`Variables 7 seen 7 expected`,
		`Clauses 12 seen 11 expected`,
		`Satisfied Clauses 12`,
		`Model satisfies the formula`,
	}
	if _out, _ok := executeChecker(satisfiableMoreThanNBClausesClausesCNF, satisfiableCompleteModel, t); _ok {
		for _i, _line := range _out {
			if len(_line) > 0 && _expected[_i] != strings.TrimSpace(_line) {
				t.Fatalf("Expected: %s / Observed: %s", _expected[_i], _line)
			}
		}
	} else {
		t.Fatal("ModelChecker execution failed")
	}
}

func TestSatisfiableLessThanNBClausesClausesCNFAndCompleteModel(t *testing.T) {
	_expected := []string{
		`Variables 7 seen 7 expected`,
		`Clauses 10 seen 11 expected`,
		`Satisfied Clauses 10`,
		`Model satisfies the formula`,
	}
	if _out, _ok := executeChecker(satisfiableLessThanNBClausesClausesCNF, satisfiableCompleteModel, t); _ok {
		for _i, _line := range _out {
			if len(_line) > 0 && _expected[_i] != strings.TrimSpace(_line) {
				t.Fatalf("Expected: %s / Observed: %s", _expected[_i], _line)
			}
		}
	} else {
		t.Fatal("ModelChecker execution failed")
	}
}

func TestSatisfiableCNFAndCompleteInconsistentModel(t *testing.T) {
	_expected := []string{
		`Variable 7 has conflicting values at line 3.`,
	}
	if _out, _ok := executeChecker(satisfiableCnf, satisfiableCompleteInconsistentModel, t); _ok {
		for _i, _line := range _out {
			if len(_line) > 0 && _expected[_i] != strings.TrimSpace(_line) {
				t.Fatalf("Expected: %s / Observed: %s", _expected[_i], _line)
			}
		}
	} else {
		t.Fatal("ModelChecker execution failed")
	}
}
