/*
 * Copyright (c) 2017, Venkatesh-Prasad Ranganath
 *
 * BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath
 */

package main

import (
	"fmt"
	"os"
	"strings"
)

func processModelLine(line string, lineNum int, lit2value map[int]bool) (
	specifiedAsSatisfied bool, status statusCode) {
	var _tokens []string
	if _tokens, status = tokenize(strings.TrimSpace(line)); status == OK && len(_tokens) > 0 {
		switch _tokens[0] {
		case "s":
			specifiedAsSatisfied = _tokens[1] == "SATISFIABLE"

		case "v":
			var _lits []int
			if _lits, status = convertToInt(_tokens[1:]); status != OK {
				fmt.Printf("Invalid line %d in model: %s\n", lineNum, line)
				return
			}

			for _, _lit := range _lits {
				if _lit == 0 {
					return
				}
				if _polarity, _exists := lit2value[_lit]; _exists && !_polarity {
					fmt.Printf("Variable %d has conflicting values "+
						"at line %d.\n", abs(_lit), lineNum)
					status = InvalidModel
				} else {
					lit2value[_lit] = true
					lit2value[-_lit] = false
				}
			}

		case "c":
			// ignore the comment

		default:
			fmt.Printf("Invalid line %d in model: %s\n", lineNum, line)
			status = InvalidFileFormat
		}
	}

	return
}

func processModel(modelFileName string) (lit2value map[int]bool,
	specifiedAsSatisfied bool, status statusCode) {
	_modelScanner, _modelFile, _err := getScanner(modelFileName)
	if _err != OK {
		os.Exit(int(_err))
	}

	defer _modelFile.Close()

	lit2value = map[int]bool{}
	lit2value[0] = true
	specifiedAsSatisfied = false
	_lineNum := 1
	for _modelScanner.Scan() {
		_line := _modelScanner.Text()
		if len(_line) > 0 {
			var _tmp1 bool
			if _tmp1, status = processModelLine(_line, _lineNum, lit2value); status == OK {
				specifiedAsSatisfied = specifiedAsSatisfied || _tmp1
			} else {
				return
			}
		}
		_lineNum++
	}

	return
}
