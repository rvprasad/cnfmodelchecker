# CNFModelChecker

Go implementation of the tool to check if the given model satisfies a given CNF formula.  It provides information about the number of seen/expected variables, seen/expected clauses, and satisfied clauses.  


## Requirements

- [Go 1.8.3](https://golang.org/dl/)


## Build

- Set GOPATH env variable to _CNFModelChecker/go_ folder.
- Set GOBIN env variable to _CNFModelChecker/go/bin_ folder.
- Execute _go-build_ script in _CNFModelChecker/go_ folder.


## Usage

- Run `modelcheck -cnf <cnf file> -model <model file>`

```
-cnf string
    CNF file in DIMACS format (http://www.satcompetition.org/2009/format-benchmarks2009.html)
-model string
    Model file in DIMACS format (http://www.satcompetition.org/2009/format-solvers2009.html)
```


## Specifics

- Ungrounded literals will be flagged.
- Unsatisfied clauses will be flagged.
- Inconsistent models will be flagged.
- A clause is deemed as satisfied if any literal in it is satisfied; independent of other literals not being grounded.
- In models, in value (_v_) lines, variables to the right of 0 will not be considered; they will not be flagged as well.
- In formulae, in clause lines, variables to the right of 0 will not be considered; they will not be flagged as well.
- All clauses and variables are processed independent of the expectation stated in parameter (_p_) line.
- Both specified/expected and seen number of variables and clauses are reported.

 
## Attribution

Copyright (c) 2017, Venkatesh-Prasad Ranganath

Licensed under BSD 3-clause "New" or "Revised" License (https://choosealicense.com/licenses/bsd-3-clause/)

Authors: Venkatesh-Prasad Ranganath
